/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.postomobconverter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 *
 * @author karp-
 */
public class PosToMobConverter {
        static String inputFile = "pos.nut";
        static String outputFile = "output.nut";
        static String mobName;
        static String convertedLine;
    public static void main(String[] args) {
    

        
        try {
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), "Windows-1250"));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "Windows-1250"));


            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("Pozycja")) {
                    String[] parts = line.split("  | ");
                    switch (parts.length) {
                        case 5:
                            {
                                mobName = parts[4].replace("//", "").trim();
                                break;
                            }
                        case 6:
                            {
                                mobName = parts[4].replace("//", "").trim();
                                break;
                            }
                        case 7:
                            {
                                mobName = parts[6].replace("//", "").trim();
                                break;
                            }
                        case 8:
                            {
                                mobName = parts[6].replace("//", "").trim();
                                break;
                            }
                        case 9:
                            {
                                mobName = parts[7].replace("//", "").trim();
                                break;
                            }
                        default:
                            {
                                mobName = "Zły Format";
                                break;
                            }
                            

                            
                    }
                    double[] coordinates = new double[3];
                    double rotation;

                    String[] posValues = parts[1].split(",");
                    for (int i = 0; i < 3; i++) {
                        posValues[i] = posValues[i].replace(",", ".");
                        coordinates[i] = Double.parseDouble(posValues[i]);
                    }

                    rotation = Double.parseDouble(parts[3].replace(",", "."));
                    
                    if (mobName == "Zły Format"){
                        writer.write("//ZŁYFORMAT");     
                    } else {      
                        
                        if (mobName == "polna") {
                            mobName = "polna bestia";
                        }
                        if (mobName == "bestia") {
                            mobName = "polna bestia";
                        }
                        if (mobName == "mlody" || mobName == "młody") {
                            mobName = "mlody UNKNOWN";
                        }
                        if (mobName == "czarny") {
                            mobName = "czarny UNKNOWN";
                        }
                        
                        
                        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
                        symbols.setDecimalSeparator('.');
                        symbols.setGroupingSeparator(',');
  
                        //dziala do obu
                    DecimalFormat decimalFormat = new DecimalFormat("0.00", symbols);
                        String formattedCoordinates = "" + decimalFormat.format(coordinates[0]) + ", "
                                + decimalFormat.format(coordinates[1]) + ", "
                                + decimalFormat.format(coordinates[2]) + ", "
                                + decimalFormat.format(rotation) + "";

                        //grupy mobów?
//                        DecimalFormat decimalFormat = new DecimalFormat("0.00", symbols);
//                        String formattedCoordinates = "[" + decimalFormat.format(coordinates[0]) + ", "
//                                + decimalFormat.format(coordinates[1]) + ", "
//                                + decimalFormat.format(coordinates[2]) + ", "
//                                + decimalFormat.format(rotation) + "]";

                            
                            String firstLetter = mobName.substring(0, 1).toUpperCase();
                            String result = firstLetter + mobName.substring(1);
                            System.out.println(result); // Wyświetli "Przykład"

                          convertedLine = String.format("preRegisterGroundHerb(\"%s\", %s);", result, formattedCoordinates); //herbsy
//                        convertedLine = String.format("addMob(\"%s\", %s)", mobName, formattedCoordinates); //moby
                                                    //addMob("Wszedzie", [-22633.00, -474.38, 27953.30, 182.76])
                                                    //preRegisterGroundHerb("Wszedzie", 13447.2,999.035,-4267.63, 315.789);
                                                    // preRegisterGroundHerb("WSZEDZIE", 10551.20, 1013.52, -9230.70, 208.57)
                                                    //Pozycja 8852.11,486.016,-5334.84 obrot 112.024  Wszedzie/

                    zamienPolskieZnaki();
                    writer.write(convertedLine);
                    
                    }
                    writer.newLine();
                    
                }
            }

            reader.close();
            writer.close();

            System.out.println("Konwersja zakończona pomyślnie. Wynik zapisano w pliku: " + outputFile);
        } catch (IOException e) {
            System.out.println("Wystąpił błąd podczas konwersji pliku: " + e.getMessage());
        }
    }
    
    private static void zamienPolskieZnaki() {
        
      convertedLine = convertedLine.replaceAll("ą", "a");
    convertedLine = convertedLine.replaceAll("ć", "c");
    convertedLine = convertedLine.replaceAll("ę", "e");
    convertedLine = convertedLine.replaceAll("ł", "l");
    convertedLine = convertedLine.replaceAll("ń", "n");
    convertedLine = convertedLine.replaceAll("ó", "o");
    convertedLine = convertedLine.replaceAll("ś", "s");
    convertedLine = convertedLine.replaceAll("ź", "z");
    convertedLine = convertedLine.replaceAll("ż", "z");

}

    
    
}
